#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"
#include <string>

class Board;
class Bishop : public Piece
{
private:
	// private methods
	bool mainDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;		// This method checks if the main diagonal way is empty
	bool secondaryDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This method checks if the secondary diagonal way is empty

public:
	Bishop(const char type, const int row, const int col);													// C'tor
	virtual ~Bishop();																						// D'tor
	virtual bool isValidMove(const int dstRow, const int dstCol) const;										// Abstract method - the method moves a piece on the board
};

#endif // !BISHOP_H