#ifndef KNIG_H
#define KIBG_H

#include "Piece.h"
#include <string>

class Board;
class King : public Piece
{
public:
	King(const char type, const int row, const int col);				// C'tor
	virtual ~King();													// D'tor
	virtual bool isValidMove(const int dstRow, const int dstCol) const;	// Abstract method - the method moves a piece on the board
};

#endif // !KNIG_H
