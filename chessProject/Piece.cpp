#include "Piece.h"
#include "Board.h"
#include <iostream>

/* STATIC VARS */
Board* Piece::_board = NULL;

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Piece::Piece(const char type, const int row, const int col): _type(type), _row(row), _col(col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Piece::~Piece() { }

/*
SET SQUARES
Setter method - _squares
input:		_squares vector 
output:		none
*/
void Piece::setSquares(Board* board) { Piece::_board = board; }

/*
SET COL
Setter col - _col
input:		col
output:		none
*/
void Piece::setCol(const int col) { this->_col = col; }

/*
SET ROW
Setter col - _row
input:		row
output:		none
*/
void Piece::setRow(const int row) { this->_row = row; }

/*
SET TYPE
Setter type - _type
input:		type
output:		none
*/
void Piece::setType(const char type) { this->_type = type; }

/*
GET TYPE
Getter method - _type
input:		none
output:		_type
*/
char Piece::getType() const { return this->_type; }

/*
GET Row
Getter method - _row
input:		none
output:		_row
*/
int Piece::getRow() const { return this->_row; }

/*
GET TYPE
Getter method - _col
input:		none
output:		_col
*/
int Piece::getCol() const { return this->_col; }

bool Piece::isCheck() const
{
	if (this->_type == WHITE_KING || this->_type == BLACK_KING) return false;
	else if (this->_type > CAPITAL_A_ASCII && this->_type < SMALL_A_ASCII)
	{
		if (this->isValidMove(this->_board->getBlackKing()->getRow(), this->_board->getBlackKing()->getCol())) return true;
	}
	else if (this->_type > SMALL_A_ASCII)
	{
		if (this->isValidMove(this->_board->getWhiteKing()->getRow(), this->_board->getWhiteKing()->getCol())) return true;
	}
	return false;
}
