#pragma once

#include <string>
#include "stdafx.h"
#include "Board.h"

class Board;
class Piece
{
protected:
    char _type;
    int _row;
    int _col;

    // static var
    static Board* _board;

public:
    Piece(const char type, const int row, const int col);                   // C'tor   
    virtual ~Piece();                                                       // D'tor
    static void setSquares(Board* board);                                   // Setter method - _squares
    void setCol(const int col);                                             // Setter method - _col
    void setRow(const int row);                                             // Setter method - _row
    void setType(const char type);                                          // Setter method - _type
    char getType() const;                                                   // Getter method - type
    int getRow() const;                                                     // Getter metohd - row
    int getCol() const;                                                     // Getter method - col
    virtual bool isValidMove(const int dstRow, const int dstCol) const = 0; // Abstract method - the method moves a piece on the board
    bool isCheck() const;                                                   // Abstract metohd - this method checks if there is a check 8)
};
