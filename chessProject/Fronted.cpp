#include "Fronted.h"

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Fronted::Fronted() :_board(Board()), _pipe(Pipe())
{
	this->_connected = this->_pipe.connect();
}

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Fronted::~Fronted() { }

/*
SEND MSG
This method send a given msg to graphics and return the answer
input:		a msg to send
output:		the msg that returned
*/
std::string Fronted::sendMsg(char* msg)
{
	this->_pipe.sendMessageToGraphics(msg);
	return this->_pipe.getMessageFromGraphics();
}

/*
IS CONNECTED
Getter method
input:		none
output:		_connected
*/
bool Fronted::isConnected() const
{
	return this->_connected;
}

/*
 TO CONNECT
This method try to connect the pipe to the graphics
input:		none
output:		none
*/
void Fronted::toConnect()
{
	this->_connected = this->_pipe.connect();
}

/*
CALCULATE CODE
This method check what is the code for a given move
input:		a string that contains the move
output:		the code
*/
char* Fronted::calculateCode(std::string msg)
{
	char msgToGraphics[1024];
	char code = char(this->_board.checkCode(msg) + 48);
	msgToGraphics[0] = code;
	msgToGraphics[1] = NULL;
	std::cout << code << std::endl;
	if (int(code) - 48 == VALID_MOVE || int(code) - 48 == CHECK)
	{
		(this->_board.getPlayer() == WHITE_PLAYER) ? this->_board.setPlayer(BLACK_PLAYER) : this->_board.setPlayer(WHITE_PLAYER);
	}
	return msgToGraphics;
}

/*
CLOSE PIPE
this method close the connection of the pipe with the graphics
input:		none
output:		none
*/
void Fronted::closePipe()
{
	this->_pipe.close();
}


