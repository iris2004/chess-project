// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#undef UNICODE
#undef _UNICODE
#include "targetver.h"

#include <iostream>
#include <stdio.h>
#include <tchar.h>
#include <vector>

#define BOARD_SIZE 8
#define SMALL_A_ASCII 97
#define CAPITAL_A_ASCII 65
#define ASCII_1 49
#define MIN_INDEX 0

enum MSG_INDEX {SRC_COL, SRC_ROW, DST_COL, DST_ROW};
enum CODES { VALID_MOVE, CHECK, INVALID_SRC, NOT_EMPTY_DST, SELF_CHECK, INVALID_INDEX, INVALID_MOVEMENT, EQUAL_SRC_DST, CHECKMATE };
enum BOARD_COLS { A, B, C, D, E, F, G, H };
enum WHITE_PIECE { WHITE_KING = 'K', WHITE_QUEEN = 'Q', WHITE_ROOK = 'R', WHITE_BISHOP = 'B', WHITE_KNIGHT = 'N', WHITE_PAWN = 'P' };
enum BLACK_PIECE { BLACK_KING = 'k', BLACK_QUEEN = 'q', BLACK_ROOK = 'r', BLACK_BISHOP = 'b', BLACK_KNIGHT = 'n', BLACK_PAWN = 'p' };

// TODO: reference additional headers your program requires here