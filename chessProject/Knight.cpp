#include "Knight.h"

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Knight::Knight(const char type, const int row, const int col) : Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Knight::~Knight() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
           false - the move is invalid
*/
bool Knight::isValidMove(const int dstRow, const int dstCol) const { return (!((this->_col + 2 == dstCol && this->_row + 1 == dstRow) || (this->_col + 2 == dstCol && this->_row - 1 == dstRow) || (this->_col + 1 == dstCol && this->_row + 2 == dstRow) || (this->_col - 1 == dstCol && this->_row + 2 == dstRow) || (this->_col + 1 == dstCol && this->_row - 2 == dstRow) || (this->_col - 1 == dstCol && this->_row - 2 == dstRow) || (this->_col - 2 == dstCol && this->_row + 1 == dstRow) || (this->_col - 2 == dstCol && this->_row - 1 == dstRow))) ? false : true; }


