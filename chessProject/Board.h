#pragma once

#include <vector>
#include "Piece.h"

// define consts
#define WHITE_PLAYER 'w'
#define BLACK_PLAYER 'b'
#define NUM_OF_PIECES 16

class Piece;
class Board
{
private:
	std::vector<std::vector<Piece*>> _squares;								// board array							
	char _player;					

	// private methods				
	void initWhitePieces();													// The method initiate all the white pieces on the board
	void initBlackPieces();													// The method initiate all the black pieces on the board				
	Piece* _blackKing;
	Piece* _whiteKing;
	bool isRival(const int col, const int row);								// This method checks if there are rivals 

public:
	Board();																// C'tor
	~Board();																// D'tor
	std::vector<std::vector<Piece*>> getSquares() const;					// Getter method - _sequares
	int checkCode(const std::string msg);									// This method returns code
	char getPlayer() const;													// Getter method - _player
	void setPlayer(const char player);										// Setter method - _player
	Piece* getWhiteKing() const;											// Getter method - _whiteKing
	Piece* getBlackKing() const;											// Getter metohd - _blackKing
	void movePieceOnBoard(int srcCol, int srcRow, int dstCol, int dstRow);	// This method moves a piece on the board (save the new place)
};