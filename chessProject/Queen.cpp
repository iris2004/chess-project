#include "Queen.h"

/*
STRAIGHT MOVEMENT
This method checks if the straight way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::straightMovement(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (dstRow != this->_row && dstCol != this->_col) return false;
	if (dstRow == this->_row) return this->moveInCol(this->_row, this->_col, dstRow, dstCol);
	return this->moveInRow(this->_row, this->_col, dstRow, dstCol);
}

/*
DIAGONAL MOVEMENT
This method checks if the diagonal way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::diagonalMovement(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (srcRow - srcCol != dstRow - dstCol && srcRow + srcCol != dstRow + dstCol) return false; // check if bishop can move as the user asked to
	if (!mainDiagonal(srcRow, srcCol, dstRow, dstCol)) return false; // main diagonal
	else return secondaryDiagonal(srcRow, srcCol, dstRow, dstCol); // secondary diagonal
}

/*
MAIN DIAGONAL
This method checks if the main diagonal way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::mainDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	// init vars
	int i = 0;
	int j = 0;

	if (dstRow > srcRow || dstCol > srcCol) for (i = srcRow + 1, j = srcCol + 1; i < dstRow || j < dstCol; i++, j++) // right
	{
		if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
		{
			return false;
		}
	}
	else for (i = srcRow - 1, j = srcCol - 1; i > dstRow || j > dstCol; i--, j--) // left
	{
		if (this->_board->getSquares()[j][i] != nullptr)
		{
			return false;
		}
	}
	return true; // if there isn't any piece in the way, so this is an empty way
}

/*
SECONDARY DIAGONAL
This method checks if the secondary diagonal way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::secondaryDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	// init vars
	int i = 0;
	int j = 0;

	if (dstRow > srcRow || dstCol > srcCol) for (i = srcRow + 1, j = srcCol - 1; i < dstRow || j > dstCol; i++, j--) // left
	{
		if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
		{
			return false;
		}
	}
	else for (i = srcRow - 1, j = srcCol + 1; i > dstRow || j < dstCol; i--, j++) // right
	{
		if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
		{
			return false;
		}
	}
	return true; // if there isn't any piece in the way, so this is an empty way
}

/*
MOVE IN COL
This method checks if the col way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::moveInCol(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (dstCol > srcCol)
	{
		for (int i = srcCol + 1; i < dstCol; i++) // right
		{
			if ((this->_board->getSquares())[i][srcRow] != nullptr)
			{
				return false;
			}
		}
	}
	else for (int i = srcCol - 1; i > dstCol; i--) // left
	{
		if ((this->_board->getSquares())[i][srcRow] != nullptr)
		{
			return false;
		}
	}
	return true;
}

/*
MOVE IN ROW
This method checks if the row way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Queen::moveInRow(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (dstRow > srcRow) for (int i = srcRow + 1; i < dstRow; i++) // forward
	{
		if ((this->_board->getSquares())[srcCol][i] != nullptr)
		{
			return false;
		}
	}
	else for (int i = srcRow - 1; i > dstRow; i--)
	{
		if ((this->_board->getSquares())[srcCol][i] != nullptr) // backward
		{
			return false;
		}
	}
	return true;
}

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Queen::Queen(const char type, const int row, const int col) : Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Queen::~Queen() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
		   false - the move is invalid
*/
bool Queen::isValidMove(const int dstRow, const int dstCol) const
{
	if ((dstRow != this->_row && dstCol != this->_col) && (this->_row - this->_col != dstRow - dstCol && this->_row + this->_col != dstRow + dstCol)) return false; // check if queen can move as the user asked to
	if (dstRow == this->_row || dstCol == this->_col) return straightMovement(this->_row, this->_col, dstRow, dstCol); // check if the user asked to move straightly
	else return diagonalMovement(this->_row, this->_col, dstRow, dstCol); // check if the user asked to move diagonally
}