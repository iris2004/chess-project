#include "King.h"
#include <cstdlib>

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
King::King(const char type, const int row, const int col): Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
King::~King() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
		   false - the move is invalid
*/
bool King::isValidMove(const int dstRow, const int dstCol) const 
{
	if ((dstRow == this->_row || dstRow == this->_row - 1 || dstRow == this->_row + 1) && (dstCol == this->_col || dstCol == this->_col + 1 || dstCol == this->_col - 1)) return true; // check if queen can move as the user asked to
	return false;
}
