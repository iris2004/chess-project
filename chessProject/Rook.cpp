#include "Rook.h"

/*
MOVE IN ROW
This method checks if the row way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Rook::moveInRow(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (dstRow > srcRow) for (int i = srcRow + 1; i < dstRow; i++) // forward
	{
		if ((this->_board->getSquares())[srcCol][i] != nullptr)
		{
			return false; 
		}
	}
	else for (int i = srcRow - 1; i > dstRow; i--)
	{
		if ((this->_board->getSquares())[srcCol][i] != nullptr) // backward
		{
			return false; 
		}
	}
	return true;
}

/*
MOVE IN COL
This method checks if the col way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Rook::moveInCol(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	if (dstCol > srcCol)
	{
		for (int i = srcCol + 1; i < dstCol; i++) // right
		{
			if ((this->_board->getSquares())[i][srcRow] != nullptr)
			{
				return false;
			}
		}
	}
	else for (int i = srcCol - 1; i > dstCol; i--) // left
	{
		if ((this->_board->getSquares())[i][srcRow] != nullptr)
		{
			return false;
		}
	}
	return true;
}

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Rook::Rook(const char type, const int row, const int col): Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Rook::~Rook() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
		   false - the move is invalid
*/
bool Rook::isValidMove(const int dstRow, const int dstCol) const 
{
	if (dstRow != this->_row && dstCol != this->_col) return false;
	if (dstRow == this->_row) return this->moveInCol(this->_row, this->_col, dstRow, dstCol);
	return this->moveInRow(this->_row, this->_col, dstRow, dstCol);
}
