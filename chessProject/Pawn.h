#ifndef PAWN_H
#define PAWN_H

#include "Piece.h"
#include <string>

class Board;

class Pawn : public Piece
{
public:
	Pawn(const char type, const int row, const int col);				// C'tor
	virtual ~Pawn();													// D'tor		
	virtual bool isValidMove(const int dstRow, const int dstCol) const;	// Abstract method - the method moves a piece on the board
};
#endif // !PAWN_H
