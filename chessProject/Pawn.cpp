#include "Pawn.h"

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Pawn::Pawn(const char type, const int row, const int col) : Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Pawn::~Pawn() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
		   false - the move is invalid
*/
bool Pawn::isValidMove(const int dstRow, const int dstCol) const
{	
	bool isFirstMove;
	if (this->_type == WHITE_PAWN)
	{
		(this->_row == 1) ? isFirstMove = true : isFirstMove = false; 
		if (dstCol == this->_col) //regular move
		{
			if (this->_board->getSquares()[dstCol][dstRow]) return false;
			if (!isFirstMove && dstRow != this->_row + 1) return false;
			if (isFirstMove && dstRow != this->_row + 1 && dstRow != this->_row + 2)  return false;
			if (isFirstMove && dstRow == this->_row + 2 && this->_board->getSquares()[dstCol][this->_row + 1])  return false;
		}
		else if ((dstCol == this->_col + 1 || dstCol == this->_col - 1)) //eat another piece
		{
			if (dstRow != this->_row + 1 || !this->_board->getSquares()[dstCol][dstRow]) return false;
		}
		else
		{
			return false;
		}
	}
	else if (this->_type == BLACK_PAWN)
	{
		(this->_row == 6) ? isFirstMove = true : isFirstMove = false;
		if (dstCol == this->_col) //regular move
		{
			if (this->_board->getSquares()[dstCol][dstRow]) return false;
			if (!isFirstMove && dstRow != this->_row - 1 ) return false;
			if (isFirstMove && dstRow != this->_row - 1 && dstRow != this->_row - 2) return false;
			if (isFirstMove && dstRow == this->_row - 2 && this->_board->getSquares()[dstCol][this->_row - 1]) return false;
		}
		else if ((dstCol == this->_col + 1 || dstCol == this->_col - 1)) //eat another piece
		{
			if (dstRow != this->_row - 1 || !this->_board->getSquares()[dstCol][dstRow]) return false; 
		}
		else
		{
			return false;
		}
	}
	return true;
}