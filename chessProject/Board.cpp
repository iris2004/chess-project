#include "Board.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "King.h"
#include "Queen.h"
#include "Bishop.h"

/*
INIT WHITE PIECES
The method initiate all the white pieces on the board
input:		none	
output:		none
*/
void Board::initWhitePieces()
{
	for (int i = 0; i < BOARD_SIZE; i++) this->_squares[i][1] = new Pawn(WHITE_PAWN, 1, i); // init pawns
	this->_squares[A][0] = new Rook(WHITE_ROOK, 0, A);
	this->_squares[B][0] = new Knight(WHITE_KNIGHT, 0, B);
	this->_squares[C][0] = new Bishop(WHITE_BISHOP, 0, C);
	this->_squares[D][0] = new King(WHITE_KING, 0, D);
	this->_squares[E][0] = new Queen(WHITE_QUEEN, 0, E);
	this->_squares[F][0] = new Bishop(WHITE_BISHOP, 0, F);
	this->_squares[G][0] = new Knight(WHITE_KNIGHT, 0, G);
	this->_squares[H][0] = new Rook(WHITE_ROOK, 0, H);}

/*
INIT BLACK PIECES
The method initiate all the black pieces on the board
input:		none
output:		none
*/
void Board::initBlackPieces()
{
	for (int i = 0; i < BOARD_SIZE; i++) this->_squares[i][6] = new Pawn(BLACK_PAWN, 6, i); // init pawns
	this->_squares[A][7] = new Rook(BLACK_ROOK, 7, A);
	this->_squares[B][7] = new Knight(BLACK_KNIGHT, 7, B);
	this->_squares[C][7] = new Bishop(BLACK_BISHOP, 7, C);
	this->_squares[D][7] = new King(BLACK_KING, 7, D);
	this->_squares[E][7] = new Queen(BLACK_QUEEN, 7, E);
	this->_squares[F][7] = new Bishop(BLACK_BISHOP, 7, F);
	this->_squares[G][7] = new Knight(BLACK_KNIGHT, 7, G);
	this->_squares[H][7] = new Rook(BLACK_ROOK, 7, H);
}

/*
IS RIVAL
This method checks if there are rivals
input:		dstRow, dstCol
output:		true or false
*/
bool Board::isRival(const int col, const int row) { return (this->_player == WHITE_PLAYER && this->_squares[col][row]->getType() > SMALL_A_ASCII && this->_squares[col][row]->getType() < 122) || (this->_player == BLACK_PLAYER && this->_squares[col][row]->getType() > CAPITAL_A_ASCII && this->_squares[col][row]->getType() < SMALL_A_ASCII); }

/*
CONSTRUCTOR
The constructor init an object
input:		none
output:		none
*/
Board::Board(): _player(WHITE_PLAYER)
{
	this->_squares.resize(BOARD_SIZE);

	for (int i = 0; i < BOARD_SIZE; i++) this->_squares[i].resize(BOARD_SIZE);
	initWhitePieces();
	initBlackPieces();
	this->_blackKing = (this->_squares[D][7]);
	this->_whiteKing = (this->_squares[D][0]);
	Piece::setSquares(this);
}

/*
DISTRUCTOR 
This method releases dynamically allocated memory
input:		none
output:		none
*/
Board::~Board() { for (int i = 0; i < BOARD_SIZE; i++) for (int j = 0; j < BOARD_SIZE; j++) delete this->_squares[i][j]; }

/*
GET SQUARES
Getter method - _squares
input:		none
output:		_squares
*/
std::vector<std::vector<Piece*>> Board::getSquares() const { return this->_squares; }

/*
CHECK CODE
This method returns code
input:		msg - src and dst msg
output:		code
*/
int Board::checkCode(const std::string msg)
{
	// init vars
	bool code = 0;
	int srcCol = int(msg[SRC_COL]) - SMALL_A_ASCII;
	int srcRow = int(msg[SRC_ROW]) - ASCII_1;
	int dstCol = int(msg[DST_COL]) - SMALL_A_ASCII;
	int dstRow = int(msg[DST_ROW]) - ASCII_1;

	std::cout << msg << std::endl;

	if (srcCol == dstCol && srcRow == dstRow) return EQUAL_SRC_DST;
	else if ((srcCol > BOARD_SIZE || srcCol < MIN_INDEX) || (srcRow > BOARD_SIZE || srcRow < MIN_INDEX) || (dstCol > BOARD_SIZE || dstCol < MIN_INDEX) || (dstRow > BOARD_SIZE || dstRow < MIN_INDEX)) return INVALID_INDEX;
	else if (!this->_squares[srcCol][srcRow] || this->isRival(srcCol, srcRow))
	{
		return INVALID_SRC;
	}
	else if (this->_squares[dstCol][dstRow] && !this->isRival(dstCol, dstRow)) return NOT_EMPTY_DST;
	else
	{
		code = this->_squares[srcCol][srcRow]->isValidMove(dstRow, dstCol);
		if (code == false) return INVALID_MOVEMENT;

		this->movePieceOnBoard(srcCol, srcRow, dstCol, dstRow);

		for (int i = 0; i < BOARD_SIZE; i++) for (int j = 0; j < BOARD_SIZE; j++)
		{		
			if (this->_squares[i][j] && this->_squares[i][j]->isCheck())
			{
				if (this->isRival(i, j) == false)
				{
					return CHECK;
				}
				else
				{
					this->movePieceOnBoard(dstCol, dstRow, srcCol, srcRow);
					std::cout << "\n" << this->_squares[i][j]->getType() << std::endl;
					std::cout << "col: " << i << " " << "row: " << j << std::endl;
					return SELF_CHECK;
				}
			}
		}
	}
	return VALID_MOVE;
}

/*
GET PLAYER
Gettr method - _player
input:        none
output:        _player
*/
char Board::getPlayer() const { return this->_player; }

/*
SET PLAYER
Setter method - _player
input:		player
output:		none
*/
void Board::setPlayer(const char player) { this->_player = player; }

/*
GET WHITE KING
Getter method - _whiteKing
input:		none
output:		_whiteKing
*/
Piece* Board::getWhiteKing() const { return this->_whiteKing; }

/*
GET BLACK KING
Getter method - _blackKing
input:		none
output:		_blackKing
*/
Piece* Board::getBlackKing() const { return this->_blackKing; }

/*
MOVE PIECE ON BOARD
This method moves a piece on the board (save the new place)
input:		srcCol, srcRow, dstCol, dstRow
output:		none
*/
void Board::movePieceOnBoard(int srcCol, int srcRow, int dstCol, int dstRow)
{
	this->_squares[dstCol][dstRow] = this->_squares[srcCol][srcRow];
	this->_squares[dstCol][dstRow]->Piece::setCol(dstCol); // set the new place
	this->_squares[dstCol][dstRow]->Piece::setRow(dstRow);
	this->_squares[srcCol][srcRow] = NULL;
}
