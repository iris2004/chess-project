#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"
#include <string>

class Board;
class Queen : public Piece
{
private:
	// private methods
	bool straightMovement(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This method checks if the straight way is empty
	bool diagonalMovement(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This metohd checks if the diagonal way is empty
	bool mainDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;		// This method checks if the main diagonal way is empty
	bool secondaryDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This method checks if the secondary diagonal way is empty
	bool moveInCol(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;			// This method checks if the col way is empty
	bool moveInRow(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;			// This method checks if the row way is empty

public:
	Queen(const char type, const int row, const int col);													// C'tor				
	virtual ~Queen();																						// D'tor
	virtual bool isValidMove(const int dstRow, const int dstCol) const;										// Abstract method - the method moves a piece on the board
};
#endif // !QUEEN_H
