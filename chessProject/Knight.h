#ifndef KNIGHT_H
#define KNIGHT_H

#include "Piece.h"
#include <string>

class Board;
class Knight : public Piece
{
public:
	Knight(const char type, const int row, const int col);				// C'tor
	virtual ~Knight();													// D'tor
	virtual bool isValidMove(const int dstRow, const int dstCol) const;	// Abstract method - the method moves a piece on the board
};

#endif // !KNIGHT_H