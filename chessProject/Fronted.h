#pragma once

#include "stdafx.h"
#include "Pipe.h"
#include "Board.h"

class Fronted
{
public:
	bool _connected;
	Pipe _pipe;
	Board _board;
public:
	Fronted();
	~Fronted();
	std::string sendMsg(char* msg);
	bool isConnected() const;
	void toConnect();
	char* calculateCode(std::string msg);
	void closePipe();
};
