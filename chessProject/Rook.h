#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"
#include <string>

class Board;
class Rook : public Piece
{
private:
	bool moveInCol(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This method checks if the col way is empty
	bool moveInRow(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const;	// This method checks if the row way is empty

public:
	Rook(const char type, const int row, const int col);											// C'tor
	virtual ~Rook();																				// D'tor
	virtual bool isValidMove(const int dstRow, const int dstCol) const;								// Abstract method - the method moves a piece on the board
};
#endif // !"ROOK_H"
