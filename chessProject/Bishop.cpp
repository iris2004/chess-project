#include "Bishop.h"
#include <iostream>


/*
MAIN DIAGONAL
This method checks if the main diagonal way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Bishop::mainDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	// init vars
	int i = 0;
	int j = 0;

	if (dstRow > srcRow || dstCol > srcCol)
	{
		for (i = srcRow + 1, j = srcCol + 1; i < dstRow && j < dstCol; i++, j++) // right
		{
			std::cout << "col: " << j << "row: " << i;
			std::cout << dstRow;
			if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
			{
				return false;
			}
		}
	}
	else for (i = srcRow - 1, j = srcCol - 1; i > dstRow || j > dstCol; i--, j--) // left
	{
		std::cout << ":C";
		if (this->_board->getSquares()[j][i] != nullptr)
		{
			return false;
		}
	}
	return true; // if there isn't any piece in the way, so this is an empty way
}

/*
SECONDARY DIAGONAL
This method checks if the secondary diagonal way is empty
input:		srcRow, srcCol, dstRow, dstCol
output:		true - empty way
			false - not empty way
*/
bool Bishop::secondaryDiagonal(const int srcRow, const int srcCol, const int dstRow, const int dstCol) const
{
	// init vars
	int i = 0;
	int j = 0;

	std::cout << "UwU";
	if (dstRow > srcRow || dstCol > srcCol)
	{
		for (i = srcRow + 1, j = srcCol - 1; i < dstRow || j > dstCol; i++, j--) // left
		{
			std::cout << ":P";
			if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
			{
				return false;
			}
		}
	}
	else for (i = srcRow - 1, j = srcCol + 1; i > dstRow || j < dstCol; i--, j++) // right
	{
		std::cout << ":O";
		if (this->_board->getSquares()[j][i] != nullptr) // check if there is any piece in the way
		{
			return false;
		}
	}
	return true; // if there isn't any piece in the way, so this is an empty way
}

/*
CONSTRUCTOR
The constructor init an object
input:		type, row, col
output:		none
*/
Bishop::Bishop(const char type, const int row, const int col) : Piece(type, row, col) { }

/*
DISTRUCTOR
This method releases dynamically allocated memory
input:		none
output:		none
*/
Bishop::~Bishop() { }

/*
IS VALID MOVE (Abstract method)
The method moves a piece on the board
input:	   msg - dst row and col
output:	   true - the move is valid
		   false - the move is invalid
*/
bool Bishop::isValidMove(const int dstRow, const int dstCol) const
{
	if (this->_row - this->_col != dstRow - dstCol && this->_row + this->_col != dstRow + dstCol) return false; // check if bishop can move as the user asked to
	if (!mainDiagonal(this->_row, this->_col, dstRow, dstCol)) return false;
	return secondaryDiagonal(this->_row, this->_col, dstRow, dstCol);
}