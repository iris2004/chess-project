#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Fronted.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
    srand(time_t(NULL));

    //init vars
    Fronted fronted = Fronted(); //create a fronted class
    char code = 0;
    string ans;

    while (!fronted.isConnected())
    {
        cout << "cant connect to graphics" << endl;
        cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
        std::cin >> ans;

        if (ans == "0") //try to connect again
        {
            cout << "trying connect again.." << endl;
            Sleep(5000);
            fronted.toConnect();
        }
        else //close the connection
        {
            fronted.closePipe();
            return 0;
        }
    }
    char msgToGraphics[1024];

    // msgToGraphics should contain the board string according to the protocol
    strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");
    string msgFromGraphics = fronted.sendMsg(msgToGraphics);   // send the board string

    while (msgFromGraphics != "quit") //plays until the user choose to quit
    {
        strcpy_s(msgToGraphics, fronted.calculateCode(msgFromGraphics));
        msgFromGraphics = fronted.sendMsg(msgToGraphics);
    }

    fronted.closePipe(); //close the connection to graphics 
}
